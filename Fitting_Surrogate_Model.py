#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bubble Detectors Surrogate Model

Created on Mon Aug 28 14:00:06 2023

@author: Benedikt Schmitz
"""

import numpy as np 
import pandas as pd 

# import surrogate modeling toolbox -- smt import
#from smt.utils.misc import compute_rms_error
#from smt.sampling_methods import LHS
from smt.surrogate_models import KRG
import matplotlib.pyplot as plt


#%% Function Definitions
# Import simulated Data (GEANT 4)
def import_GEANT4_Data(path='Data/Geant4_Sim_Bubble_Response.xlsx'):
    """
    Imports the Simulation Data from the supplied files by M. Koslowski (bubbletec)

    Parameters
    ----------
    path : TYPE, optional
        DESCRIPTION. The default is 'Data/Geant4_Sim_Bubble_Response.xlsx'.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    sim_data = pd.read_excel(path,sheet_name=0)
    sim_erro = pd.read_excel(path,sheet_name=1)
    ### We are going to create a large numpy array which will be filled with nans, since they have to be delted later anyway.
    ### 0s have physical meaning in this case so we can not use that one.
    length_of_array = (sim_data[sim_data.columns[0]].shape[0] -1)*len(sim_data.columns) # The heading is counted as well, so subtract 1; The value then is multiplied by the number of columns
    empty_sim_array = np.empty((length_of_array,4))
    empty_sim_array.fill(np.nan)
    
    length_of_array = (sim_erro[sim_erro.columns[0]].shape[0] -1)*len(sim_erro.columns) # The heading is counted as well, so subtract 1; The value then is multiplied by the number of columns
    empty_err_array = np.empty((length_of_array,4))
    empty_err_array.fill(np.nan)
    
    
    ### We loop over the columns: Simulation Data
    ### The 0th column gives the header, it can be either nan, "Energy (MeV)", or the threshold / stoppnig power value in keV/micron
    for key in sim_data.columns:
        if sim_data[key][0] == "Energy (MeV)":
            x_sim_temp = sim_data[key][1:].to_numpy()
    array_index = 0
    for key in sim_data.columns:
        if sim_data[key][0] != "Energy (MeV)" and sim_data[key][0].dtype == 'float64':
            y_sim_data = sim_data[key][1:].to_numpy()
            x_sim_threshold = np.ones(y_sim_data.shape)*sim_data[key][0]
            # Write data in the array:
            empty_sim_array[array_index:array_index+(x_sim_temp.shape[0]),0]=x_sim_temp
            empty_sim_array[array_index:array_index+(x_sim_temp.shape[0]),1]=x_sim_threshold
            empty_sim_array[array_index:array_index+(x_sim_temp.shape[0]),2]=y_sim_data
                
            array_index = array_index+x_sim_temp.shape[0]
    
    ### Final step: Delete all nan rows
    nan_index = np.argwhere(np.isnan(empty_sim_array[:,2]))
    sim_array = np.delete(empty_sim_array, (nan_index), axis=0)
    #sim_array[:,3] = 0.1#np.sqrt(sim_array[:,2]) # Uncertainty estimation as 10 percent
    
    ### We loop over the columns: Error Data
    ### The 0th column gives the header, it can be either nan, "Energy (MeV)", or the threshold / stoppnig power value in keV/micron
    for key in sim_erro.columns:
        if sim_erro[key][0] == "REL ERROR":
            x_sim_temp = sim_erro[key][1:].to_numpy()
    array_index = 0
    for key in sim_erro.columns:
        if sim_erro[key][0] != "REL ERROR" and (sim_erro[key][0].dtype == 'float64' or sim_erro[key][0].isnan()):
            y_sim_data = sim_erro[key][1:].to_numpy()
            x_sim_threshold = np.ones(y_sim_data.shape)*sim_erro[key][0]
            # Write data in the array:
            empty_err_array[array_index:array_index+(x_sim_temp.shape[0]),0]=x_sim_temp
            empty_err_array[array_index:array_index+(x_sim_temp.shape[0]),1]=x_sim_threshold
            empty_err_array[array_index:array_index+(x_sim_temp.shape[0]),2]=y_sim_data
                
            array_index = array_index+x_sim_temp.shape[0]
    
    ### Final step: Delete all nan rows
    nan_index = np.argwhere(np.isnan(empty_err_array[:,2]))
    err_array = np.delete(empty_err_array, (nan_index), axis=0)
    err_array[:,3] = 0.1#np.sqrt(sim_array[:,2]) # Uncertainty estimation as 10 percent
    
    # We want to join both arrays to deal with it better. 
    # First two columns should be the the same, then the error values can be passed 
    # to the last column of the simulation data.
    
    bool_array = sim_array == err_array
    if bool_array[:,0].all() and bool_array[:,1].all():
        sim_array[:,3]=err_array[:,2]
        return sim_array
    else:
        print('Arrays do not match. Returning both.')
        return sim_array, err_array

# including the "measurement uncertainty" 
# The experimental data has an asymmetric uncertainty, this is dealt with by taking the maximum uncertainty for each point
def calculate_relative_measurement_uncertainty(Data_En_Curve):
    Relative_Error_High = abs((Data_En_Curve['Value']-Data_En_Curve['High Bound '])/Data_En_Curve['Value'])
    Relative_Error_Low  = abs((Data_En_Curve['Value']-Data_En_Curve['Low Bound'])/Data_En_Curve['Value'])
    Boolean_Test = Relative_Error_High > Relative_Error_Low
    Relative_Error = np.zeros(Relative_Error_High.shape)
    for idx, x in enumerate(Boolean_Test):
        if x == True:
            Relative_Error[idx] = Relative_Error_High[idx]
        elif x== False: 
            Relative_Error[idx] = Relative_Error_Low[idx]
    
    return Relative_Error 

# Resample function for uncertainty quantification
def resample_data(data_array, nres=5):
    # Preallocate Result Array with nans
    DataArray = np.empty((data_array.shape[0], data_array.shape[1]-1+nres))    
    DataArray.fill(np.nan)
    # Fill the X-columns:
    DataArray[:,0] = data_array[:,0]
    DataArray[:,1] = data_array[:,1]
    # Fill the Original Y-Column
    DataArray[:,2] = data_array[:,2]
    # Now resampling and filling of the other columns
    # Randomized Resampling # nres*data_array length
    resampled_data = np.random.default_rng(seed=42).normal(0, 1, nres * data_array.shape[0])    
    # Now order the data points: 
    # Iterate over the full DataFrame:
    for i in range(data_array.shape[0]):
        # resample nres times
        values = resampled_data[i*nres:(i+1)*nres]
        resample_data = data_array[i,2] + values * data_array[i,2] * data_array[i,3]
        # merge data
        DataArray[i,3:] = resample_data
    # Return the resampled array
    return DataArray
#%% Routine 
if __name__ == "__main__":
    
    # import experimental data 
    Data_En_Curve = pd.read_csv('Data/Experimental_ResponseData_Curve.csv')
    # import simulated data 
    sim_data = import_GEANT4_Data(path='Data/Geant4_Sim_Bubble_Response.xlsx')
    
    # Add an additional systematic uncertainty on the simulation data (Given as relative error)
    sys_uncert = 0.10 # This means 10 Percent
    sim_data[:,3] = sim_data[:,3] + sys_uncert
    
    # Construct the experimental data array
    exp_data = np.array([Data_En_Curve['Energy / MeV'].to_numpy(), np.ones(Data_En_Curve['Energy / MeV'].to_numpy().shape)*120, Data_En_Curve['Value'].to_numpy(), np.zeros(Data_En_Curve['Energy / MeV'].to_numpy().shape)]).T 
    # Calculate the relative measurement uncertainty
    exp_data[:,3] = calculate_relative_measurement_uncertainty(Data_En_Curve = Data_En_Curve)
    

#%% Plotting of both data sets
    if False: # Set to True to plot data
        plt.figure()
        
        threshold_list = list(set(sim_data[:,1]))
        threshold_list.sort()
        
        for threshold in threshold_list[9:10]:
            # find all data points with the selected threshold
            data_ind = np.where(sim_data[:,1]==threshold)
            # select only these points 
            tempData = sim_data[data_ind,:].squeeze()
            
            y_error=np.squeeze(np.vstack(tempData[:,2]*tempData[:,3]))
            
            plt.errorbar(tempData[:,0],tempData[:,2],yerr=y_error,fmt='x')#,yerr=tempData[:,2]*tempData[:,3],label=str(threshold))
            plt.yscale('log')
            plt.xscale('log')
        
        xt = np.log10(Data_En_Curve['Energy / MeV'].to_numpy())
        yt = np.log10(Data_En_Curve['Value'].to_numpy())
        y_error=np.vstack((Data_En_Curve['Value'] - Data_En_Curve['Low Bound'], Data_En_Curve['High Bound '] - Data_En_Curve['Value']))
        
        plt.errorbar(exp_data[:,0], exp_data[:,2], yerr=y_error, fmt='r.', label='Data');
        plt.xlabel('Neutron Energy / MeV');
        plt.ylabel('Response / bubble / n / cm²');
#%% Preprocessing and data sorting
    nres=20 # number of resamplings to be done
    NewDataSim = resample_data(data_array=sim_data, nres=nres)
    NewDataExp = resample_data(data_array=exp_data, nres=nres)

    ## Check whether log10 should be applied on the data before regression    
    x_is_log = True
    y_is_log = True
    # Datapoints have to be bloated up
    ## The simulation
    x_train_Sim = np.zeros((NewDataSim.shape[0]*nres,2))
    y_train_Sim = np.zeros((NewDataSim.shape[0]*nres,1))
    for i in range(NewDataSim.shape[0]):
        for j in range(nres):
            if x_is_log:
                x_train_Sim[i*nres+j,0] = np.log10(NewDataSim[i,0])
            else: 
                x_train_Sim[i*nres+j,0] = NewDataSim[i,0]
            x_train_Sim[i*nres+j,1] = NewDataSim[i,1]
            y_train_Sim[i*nres+j,0] = NewDataSim[i,j+2]
            
    ### Cut the zeros
    if y_is_log:
        x_train_Sim = x_train_Sim[np.squeeze(y_train_Sim > 0),:]
        y_train_Sim = np.log10(y_train_Sim[np.squeeze(y_train_Sim > 0)])
    
    ## The experimental Data
    x_train_Exp = np.zeros((NewDataExp.shape[0]*nres,2))
    y_train_Exp = np.zeros((NewDataExp.shape[0]*nres,1))
    for i in range(NewDataExp.shape[0]):
        for j in range(nres):
            if x_is_log:
                x_train_Exp[i*nres+j,0] = np.log10(NewDataExp[i,0])
            else:
                x_train_Exp[i*nres+j,0] = NewDataExp[i,0]
            x_train_Exp[i*nres+j,1] = NewDataExp[i,1]
            if y_is_log:
                y_train_Exp[i*nres+j,0] = np.log10(NewDataExp[i,j+2])
            else:
                y_train_Exp[i*nres+j,0] = NewDataExp[i,j+2]
    #%% Setup model and train
    which_model = 'sim_only'#,'exclude_sim','' 
    # 'sim_only' does not consider experimental data at all, 
    # 'exclude_sim' only takes experimental data and fills up with simulations outside of the experimental range
    # '' just takes all data 
    ndim = 1
    sm = KRG(use_het_noise=True, eval_noise=True,theta0=[1e-2]*ndim,print_prediction = True)
    
    # low-fidelity dataset names being integers from 0 to level-1
    x_sim = x_train_Sim[x_train_Sim[:,1]==120,0]
    y_sim = y_train_Sim[x_train_Sim[:,1]==120]
    #sm.set_training_values(x_sim, y_sim, name=0)
    if which_model == 'exclude_sim':
        # exclude simulation data in the range of the experimental data
        x_sim_red = x_sim[(x_sim < min(x_train_Exp[:,0]))+(x_sim > max(x_train_Exp[:,0]))]
        y_sim_red = y_sim[(x_sim < min(x_train_Exp[:,0]))+(x_sim > max(x_train_Exp[:,0]))]
        sm.set_training_values(np.hstack((x_train_Exp[:,0],x_sim_red)), np.vstack((y_train_Exp,y_sim_red)))
    elif which_model == 'sim_only':
        # only simulation data
        sm.set_training_values(x_sim, y_sim)
    else:
        which_model=''
        # high-fidelity dataset without name
        sm.set_training_values(np.hstack((x_train_Exp[:,0],x_sim)), np.vstack((y_train_Exp,y_sim)))
    
    
    sm.train()

#%% Verfication data and prepare plot
    n_samples=200 
    x = np.zeros((n_samples,ndim))
    if x_is_log:
        x[:,0] = np.log10(np.logspace(-1.2, 3, n_samples))
    elif x_is_log == False:
        x[:,0] = np.linspace(0.1, 200, n_samples)
    #x[:,1] = 120
    
    y = sm.predict_values(x)
    var = sm.predict_variances(x)

#%% Plot the model
    ## Global settings: 
    alpha_val = 0.3
    plotted_sigmas = 2
    
    plt.figure(dpi=200)
    plt.rcParams["font.size"] = "13"
    if x_is_log and y_is_log:
        plt.plot(10**x_train_Exp[:,0], 10**y_train_Exp,'kx',alpha=alpha_val, label='Exp Data')
        plt.plot(10**x_sim, 10**y_sim,'+',alpha=alpha_val, label='Sim Data')
        plt.plot(10**x[:,0], 10**y,'r-', label='Model')
        
        plt.fill_between(
            np.ravel(10**x),
            np.ravel(10**(y - plotted_sigmas * np.sqrt(var))),
            np.ravel(10**(y + plotted_sigmas * np.sqrt(var))),
            color="lightgrey",
            label=str(plotted_sigmas)+'$\sigma$'
        )
        plt.plot(np.ravel(10**x), np.ravel(10**(y - plotted_sigmas * np.sqrt(var))),'--',color='grey')
        plt.plot(np.ravel(10**x), np.ravel(10**(y + plotted_sigmas * np.sqrt(var))),'--',color='grey')
        
        # set the axis limits
        plt.xlim([min(10**x[:,0]),max(10**x[:,0])])
        
    elif x_is_log == False and y_is_log == False:
        plt.plot(x_train_Exp[:,0], y_train_Exp,'kx',alpha=alpha_val, label='Exp Data')
        plt.plot(x_sim, y_sim,'+',alpha=alpha_val, label='Sim Data')
        plt.plot(x[:,0], y,'r-', label='Model')
        
        plt.fill_between(
            np.ravel(x),
            np.ravel((y - plotted_sigmas * np.sqrt(var))),
            np.ravel((y + plotted_sigmas * np.sqrt(var))),
            color="lightgrey", 
            label=str(plotted_sigmas)+'$\sigma$'
        )
        plt.plot(np.ravel(x), np.ravel(y - plotted_sigmas * np.sqrt(var)),'--',color='grey')
        plt.plot(np.ravel(x), np.ravel(y + plotted_sigmas * np.sqrt(var)),'--',color='grey')
        
        # set the axis limits
        plt.xlim([min(10**x_train_Exp[:,0]),max(10**x_train_Exp[:,0])])
    else:
        print('Plot not defined')
        
    plt.legend()
    plt.grid()
    # overwrite ylim:
    plt.ylim([1E-10,1.5E-4])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Neutron Energy in MeV', fontsize=15)#, weight="bold")
    plt.ylabel('Response in bubble / n / cm²', fontsize=15)#, weight="bold")
    
    if False:
        plt.savefig("Model_"+which_model+"120keV.pdf", bbox_inches="tight", pad_inches=0)
    
#%% Save the model to a pickle file
    if False:
        import pickle as pkl
        filename = "BubbleModel_"+which_model+str(int(sys_uncert*100))+".pkl"
        with open(filename, "wb") as f:
           pkl.dump(sm, f)
