# Bubble Detector Evaluation Skripts and Surrogate Model

[![License: Attribution 4.0](https://img.shields.io/badge/License-cc-Attribution4.svg)](https://git.rwth-aachen.de/surrogat-models/bubbledetectors/-/blob/main/LICENSE)
<!-- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868) -->

## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Folder Tree](#folderstructure)
3. [Known Problems](#bugs)
4. [Publications & Links](#publications)
5. [Contributers](#contributors)
6. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository cotains the scripts and necessary data for the article on bubble detectors. 


## 2. Structure of Folder Tree <a name="folderstructure"></a>

The files contain the following:
* `Jung_Method.ipynb`: Jupyter Notebook to deal with the reference model from Jung et al.
* `BD_Analysis.ipynb`: Jupyter Notebook to calculate the full BD Analysis. 
* `Plotting.ipynb`: Jupyter Notebook for the generation of the article plots. 
* `Fitting_Surrogate_Model.py`: Python script to calculate and fit the surrogate model to the given data.
* `Data/BTI_Data`: Folder that contains the training data extracted and supplied by BTI.
* `Data/Experiment_Data`: Data from the HZDR beamtime..
* `Data/Jung_Response`: Extracted model data from Jung et al.
* `Data/Surrogate_Model`: Contains the pickle files with the different surrogate models evaluated in the study.
* `Data/Monte_Carlo`: Contains the results of the PHITS simulations for neutron yield used in the study.

## 4. Known Problems <a name="bugs"></a>
No known problems.


## 5. Publications & Links <a name="publications"></a>


## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   S. Scheuren, 
*   B. Schmitz

## 7. Funding <a name="funding"></a>
Refer to the articles funding paragraph.
